import * as THREE from './three.module.js';
import { ARButton } from './ARButton.js';

let camera, scene, renderer;
let controller;

init();
animate();

function init() {
    // изграждаме платното в HTML файла, върху което ще се изобразят обектите
	const container = document.createElement( 'div' );
	document.body.appendChild( container );
	
	// създаваме сцена
	scene = new THREE.Scene();
	
	// създаваме камера
	camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 20 );
	
	// създаваме светлина
	const light = new THREE.HemisphereLight( 0xffffff, 0xbbbbff, 1 );
	light.position.set( 0.5, 1, 0.25 );
	// и я добавяме към сцената
	scene.add( light );
	
	// определяме рендера
	renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	// и казваме, че искаме да поддържа AR
	renderer.xr.enabled = true;
	container.appendChild( renderer.domElement );
    
    // създаваме бутона за задействане на AR
	document.body.appendChild( ARButton.createButton( renderer ) );

    // определяме геометрията за конус
	const geometry = new THREE.CylinderGeometry( 0, 0.05, 0.2, 32 ).rotateX( Math.PI / 2 );

    // създаваме функцията, с която да се създават конуси при кликване върху дисплея
	function onSelect() {
		const material = new THREE.MeshPhongMaterial( { color: 0xffffff * Math.random() } );
		const mesh = new THREE.Mesh( geometry, material );
		mesh.position.set( 0, 0, - 0.3 ).applyMatrix4( controller.matrixWorld );
		mesh.quaternion.setFromRotationMatrix( controller.matrixWorld );
		scene.add( mesh );
	}

    // създаваме контролер за AR
	controller = renderer.xr.getController( 0 );
	controller.addEventListener( 'select', onSelect );
	scene.add( controller );

	window.addEventListener( 'resize', onWindowResize );
}

// определяме функцията за преоразмеряване на прозореца
function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
}

// определяме функцията за анимиране
function animate() {
	renderer.setAnimationLoop( render );
}

// определяме функцията за рендериране
function render() {
	renderer.render( scene, camera );
}